# README #

Hello, Sir Tope.

### Abstract ###

CS 180 MP of Franklin David Ang, Kelly Co and J. Stephen Mariano of section WFW. Uses the A* search algorithm to automate a maze game, which was made for CS 145.

### Getting started ###

This project is somewhat difficult to build without an IDE or dedicated build tool, as the game it is based on has several packages with cross-dependencies. A pre-built version has been included. The following instructions discuss how to run the MP using that.

1. Download the folder "build" in the source files.
1. Navigate to folder "classes" inside "build".
1. Inside "classes", open a command prompt (or three).
1. Open a server. You may specify the port as an argument. Defaults to 8888.
1. Connect at least one client. You may specify the IP address and port as an argument. Defaults to 127.0.0.1:8888.
1. Enter anything in the server to start the game (and the clients' frontends).
```
#!bash
$ java liquidcocaine.server.Server 8080
$ java liquidcocaine.client.Client 192.168.254.101:8080
```

### Gameplay ###

This was a fairly simple game when demoed for CS 145. It is made even simpler by the automation via A*.

* The game can be played by one or more players. Each player is represented as a colored circular token on the map. In a client's window, their piece is indicated by a small white circle in the center.
* The game takes place on a constantly changing map, which can have passable (light) tiles or impassable (dark) tiles.
* The objective of the game is to be the first to find a clear path to the golden goal tile at the center.
* A clear path is defined as a sequence of passable tiles from the player's token to the goal tile, with each tile being horizontally or vertically adjacent to the last (i.e. no diagonal moves).
* When a player thinks they have found a clear path, they can press a number key from 1-3, or the spacebar. This runs the A* algorithm to check for a clear path. 
* The number keys designate different heuristics to the A* algorithm, while the spacebar defaults to Manhattan distance.

1. Manhattan distance
1. Euclidean distance
1. Sum of Manhattan and Euclidean distance

* The expansion of the A* tree is displayed ingame with explored tiles being highlighted red.
* If the execution of the A* ends in the goal tile, the player wins. Otherwise, the player loses or is eliminated. In a game with more than 2 players, the others can continue the game after a player has been eliminated.