package liquidcocaine.base;

import java.util.HashMap;
import liquidcocaine.util.ThreadHelper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public abstract class LiquidNode implements LiquidRunnable {
  
  public HashMap<String, LiquidModule> modules;
  public int port;
  
  public LiquidNode() {
    modules = new HashMap();
  }
  
  public void receiveDatagram(Datagram datagram) {
    if (datagram != null) datagram.execute(this);
  }
  
  public void teardown() {
    for (LiquidModule module : modules.values())
      module.teardown();
  }
  
  public void addModule(LiquidModule module, String name) {
    modules.put(name, module);
    ThreadHelper.start(module, name);
  }
  
  public void removeModule(String name) {
    modules.get(name).teardown();
    modules.remove(name);
  }
  
}
