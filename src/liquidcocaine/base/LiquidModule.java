package liquidcocaine.base;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public abstract class LiquidModule implements LiquidRunnable {
  
  protected LiquidNode node;
  protected volatile boolean good;
  
  public LiquidModule(LiquidNode node) {
    this.node = node;
    good = true;
  }
  
  public void run() {
    setup();
    while (good)
      loop();
    cleanup();
  }
  
  public abstract void loop();
  public void setup() { }
  public void cleanup() { }
  
  public void teardown() {
    good = false;
  }

}
