package liquidcocaine.base;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public interface LiquidRunnable extends Runnable {
  public void teardown();
}
