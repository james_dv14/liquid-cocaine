package liquidcocaine.base;

import java.io.Serializable;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public interface Datagram extends Serializable {
  public void execute(LiquidNode ln);
}
