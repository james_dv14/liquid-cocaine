package liquidcocaine.game;

import java.awt.event.KeyEvent;
import javax.swing.JOptionPane;
import liquidcocaine.astar.HeuristicManhattan;
import liquidcocaine.astar.LiquidSolver;
import liquidcocaine.client.Client;
import liquidcocaine.server.ServerDatagram;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class GameController {
  //***************************************
  // Model
  //***************************************
  Client client;
  GameModel model;
  //***************************************
  // Constructor
  //***************************************
  public GameController(Client c, GameModel gm) {
    client = c;
    model = gm;
  }
  //***************************************
  // Game events
  //***************************************
  public void resolveMove(int x, int y) {
    if (model.interactive) {
      client.send(new ServerDatagram.DisplacePlayer(client.id, x, y));
    }
  }
  //***************************************
  // General event handlers
  //***************************************
  public void resolveKeyTyped(KeyEvent e) {
    
  }
  
  public void resolveKeyPressed(KeyEvent e) {
    int h;
    try {
      h = Integer.parseInt(e.getKeyChar() + "");
    }
    catch (NumberFormatException ex) {
      h = 1;
    }
    client.send(new ServerDatagram.PerformAStar(client.id, h));
    /*
      if (e.getKeyCode() == KeyEvent.VK_L)
       client.send(new ServerDatagram.ToggleLock());
      if (e.getKeyCode() == KeyEvent.VK_UP || e.getKeyCode() == KeyEvent.VK_W)
        resolveMove(0, -1);
      if (e.getKeyCode() == KeyEvent.VK_DOWN || e.getKeyCode() == KeyEvent.VK_S)
        resolveMove(0, 1);
      if (e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_A)
        resolveMove(-1, 0);
      if (e.getKeyCode() == KeyEvent.VK_RIGHT || e.getKeyCode() == KeyEvent.VK_D)
        resolveMove(1, 0);
    */
  }
  
  public void resolveKeyReleased(KeyEvent e) {
    
  }

}