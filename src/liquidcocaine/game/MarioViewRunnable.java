package liquidcocaine.game;

import liquidcocaine.game.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.Random;
import liquidcocaine.base.*;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class MarioViewRunnable extends LiquidModule {
  // fields
  Random rand;
  Graphics graphics;
  Graphics2D g2d;
  Color background;

  // Variables for counting frames per seconds
  int fps;
  int frames;
  long totalTime;
  long curTime;
  long lastTime;
  
  private final MarioView mw;
  
  public MarioViewRunnable(LiquidNode ln, MarioView mw) {
    super(ln);
    this.mw = mw;
    good = true;
  }
  
  @Override
  public void setup() {
    rand = new Random();
    graphics = null;
    g2d = null;
    background = Color.BLACK;

    // Variables for counting frames per seconds
    fps = 0;
    frames = 0;
    totalTime = 0;
    curTime = System.currentTimeMillis();
    lastTime = curTime;
  }

  public void loop() {
    try {
      // count Frames per second...
      lastTime = curTime;
      curTime = System.currentTimeMillis();
      totalTime += curTime - lastTime;
      if( totalTime > 1000 ) {
        totalTime -= 1000;
        fps = frames;
        frames = 0;
      }
      ++frames;

      // clear back buffer
      g2d = mw.bi.createGraphics();
      g2d.setColor(background);
      g2d.fillRect(0,0,640,480);

      // display frames per second...
      if(mw.fpsFlag) {
        g2d.setFont( new Font( "Courier New", Font.PLAIN, 12 ) );
        g2d.setColor( Color.GREEN );
        g2d.drawString( String.format( "FPS: %s", fps ), 20, 20 );
      }

      // draw stuff
      synchronized(mw.gameobjects) {
        for (GameObject go : mw.gameobjects)
          go.paint(g2d);
      }

      // placing the back buffer in front
      graphics = mw.buffer.getDrawGraphics();
      graphics.drawImage(mw.bi,0,0,null);
      if (!mw.buffer.contentsLost()) mw.buffer.show();
      Thread.yield();
    } 
    finally {
      if (graphics != null) graphics.dispose();
      if (g2d != null) g2d.dispose();
    }
  }
}
