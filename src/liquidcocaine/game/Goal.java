package liquidcocaine.game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import static liquidcocaine.game.Tile.graphics;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Goal extends Tile {
  BufferedImage image;
  
  public Goal(int x, int y) {
    super(x, y);
    this.image = MarioView.getImage("assets/goal.png");
  }
  
  @Override
  public void paint(Graphics2D g2d) {
    if (graphics) {
      g2d.drawImage(image, xPx(), yPx(), null);
    }
    else {
      g2d.setColor(new Color(224,224,0));
      g2d.fillRect(xPx(), yPx(), GameMap.TILE_SIZE, GameMap.TILE_SIZE);
    }
  }

}
