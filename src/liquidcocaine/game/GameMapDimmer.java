package liquidcocaine.game;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class GameMapDimmer implements GameObject {
  Color c;
  
  public GameMapDimmer() {
    c = new Color(0, 0, 0, 223);
  }
  
  @Override
  public void paint(Graphics2D g2d) {
    g2d.setColor(c);
    g2d.fillRect(GameMap.X_OFFSET, GameMap.Y_OFFSET, GameMap.SIZE, GameMap.SIZE);
  }

}
