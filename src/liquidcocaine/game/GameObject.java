package liquidcocaine.game;

import java.awt.Graphics2D;
import java.io.Serializable;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public interface GameObject extends Serializable {
  public void paint(Graphics2D g2d);
}
