package liquidcocaine.game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import static liquidcocaine.game.GameMap.X_OFFSET;
import static liquidcocaine.game.GameMap.Y_OFFSET;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Obstacle extends Tile {
  
  BufferedImage image;
  AffineTransform t;
  
  public Obstacle(int x, int y) {
    super(x, y);
    BufferedImage sheet = MarioView.getImage("assets/obstacles.png");
    int row = (int)(Math.random() * 2);
    int col = (int)(Math.random() * 5);
    image = sheet.getSubimage(
      GameMap.TILE_SIZE * col,
      GameMap.TILE_SIZE * row,
      GameMap.TILE_SIZE,
      GameMap.TILE_SIZE
    );
  }
  
  @Override
  public void paint(Graphics2D g2d) {
    if (graphics) {
      g2d.drawImage(image, xPx(), yPx(), null);
    }
    else {
      g2d.setColor(new Color(33,33,33));
      g2d.fillRect(xPx(), yPx(), GameMap.TILE_SIZE, GameMap.TILE_SIZE);
    }
  }

}
