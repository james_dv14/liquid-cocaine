package liquidcocaine.game;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public abstract class Tile implements GameObject {
  public static boolean graphics = false;
  
  public int x_tile;
  public int y_tile;
  
  public Tile(int x, int y) {
    x_tile = x;
    y_tile = y;
  }
  
  public int xPx() {
    return x_tile * GameMap.TILE_SIZE + GameMap.X_OFFSET;
  }
  
  public int yPx() {
    return y_tile * GameMap.TILE_SIZE + GameMap.Y_OFFSET;
  }
  
}
