package liquidcocaine.game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import liquidcocaine.util.ColorHelper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class PlayerToken extends Tile {  
  BufferedImage image;
  Color c;
  
  public PlayerToken(int x, int y, Color c) {
    super(x, y);
    this.image = MarioView.getImage("assets/token.png");
    this.c = c;
  }
  
  public PlayerToken(int x, int y) {
    this(x, y, ColorHelper.getRandomColor());
  }
  
  @Override
  public void paint(Graphics2D g2d)  {
    if (graphics) g2d.drawImage(image, xPx(), yPx(), null);
    g2d.setColor(c);
    g2d.fillArc(xPx(), yPx(), GameMap.TILE_SIZE, GameMap.TILE_SIZE, 0, 360);
  }

}