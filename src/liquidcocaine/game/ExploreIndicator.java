package liquidcocaine.game;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ExploreIndicator extends Tile {
  Color c;
  
  public ExploreIndicator(int x, int y) {
    super(x, y);
    c = new Color(255, 0, 0, 127);
  }
  
  @Override
  public void paint(Graphics2D g2d) {
    g2d.setColor(c);
    g2d.fillRect(xPx(), yPx(), GameMap.TILE_SIZE, GameMap.TILE_SIZE);
  }

}
