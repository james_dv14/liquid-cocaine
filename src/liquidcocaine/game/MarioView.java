package liquidcocaine.game;


import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.Vector;
import javax.swing.*;
import javax.imageio.*;
import java.io.*;
import liquidcocaine.base.LiquidNode;
import liquidcocaine.client.Client;

public class MarioView extends JFrame implements KeyListener {
  GameModel model;
  GameController controller;
  MarioViewRunnable mvr;
  
  Vector<GameObject> gameobjects = new Vector<GameObject>();
  public static int WIDTH = 800;
  public static int HEIGHT = 600;
  public static int CANVAS_X = 640;
  public static int CANVAS_Y = 480;
  public static final int REFRESH_RATE = 20;

  boolean fpsFlag = true;

  Canvas canvas;
  BufferStrategy buffer;
  GraphicsEnvironment ge;
  GraphicsDevice gd;
  GraphicsConfiguration gc;

  BufferedImage bi;
  LiquidNode ln;

  public MarioView(LiquidNode ln) {
    this.ln = ln;
    this.setTitle("Liquid Cocaine (powered by MarioView)");
    this.setLocation(100,100);
    this.setIgnoreRepaint(true);
    this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    
    WindowListener exitListener = new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        int dialog_result = JOptionPane.showOptionDialog(
          null, "Are you wish to quit? Quitters never win!", 
          "Exit Confirmation", JOptionPane.YES_NO_OPTION, 
          JOptionPane.QUESTION_MESSAGE, null, null, null
        );
        if (dialog_result == JOptionPane.YES_OPTION) {
          dispose();
        }
      }
    };
    this.addWindowListener(exitListener);
    
    this.addKeyListener(this);

    canvas = new Canvas();
    canvas.setIgnoreRepaint(true);
    canvas.setSize(CANVAS_X, CANVAS_Y);

    this.add(canvas);
    this.pack();
    this.setVisible(true);

    // using back buffering
    canvas.createBufferStrategy(2);
    buffer = canvas.getBufferStrategy();

    // getting the graphics configuration
    ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    gd = ge.getDefaultScreenDevice();
    gc = gd.getDefaultConfiguration();

    // creating the back buffer
    bi = gc.createCompatibleImage(CANVAS_X, CANVAS_Y);
  }

  public void showFPS(boolean fpsFlag) {
    this.fpsFlag = fpsFlag;
  }
  
  public void setModel(GameModel gm) {
    model = gm;
    Client c = (Client)ln;
    controller = new GameController(c, gm);
  }

  public void add(GameObject go) {
    gameobjects.add(go);
  }
  
  public void remove(GameObject go) {
    gameobjects.remove(go);
  }
  
  public void keyTyped(KeyEvent e) {
    controller.resolveKeyTyped(e);
  }

  public void keyPressed(KeyEvent e) {
    controller.resolveKeyPressed(e);
    
  }

  public void keyReleased(KeyEvent e) {
    controller.resolveKeyReleased(e);
  }
  
  public void display() {
    mvr = new MarioViewRunnable(ln, this);
    Thread t = new Thread(mvr);
    t.setName("MarioWindow draw");
    t.start();
  }
  
  public void dispose() {
    System.exit(0);
    mvr.teardown();
    super.dispose();
  }

  public static BufferedImage getImage(String filename) {
    try {
      File fp = new File(filename);
      BufferedImage img = ImageIO.read(fp);
      return img;
    } 
    catch (Exception e) {
      System.out.println("Unable to read file!");
      return null;
    }
  }
  
}