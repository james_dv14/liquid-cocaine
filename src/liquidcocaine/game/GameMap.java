package liquidcocaine.game;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class GameMap implements GameObject {
  public static final int SIZE = 475;
  public static final int TILE_SIZE = 25;
  public static final int SIZE_IN_TILES = SIZE / TILE_SIZE;
  public static final int CENTER_TILE = SIZE_IN_TILES / 2;
  public static final int X_OFFSET = (MarioView.CANVAS_X - SIZE) / 2;
  public static final int Y_OFFSET = (MarioView.CANVAS_Y - SIZE) / 2;
  
  BufferedImage image;
  
  public GameMap() {
    this.image = MarioView.getImage("assets/map.png");
  }
  
  @Override
  public void paint(Graphics2D g2d) {
    if (Tile.graphics) {
      g2d.drawImage(image, X_OFFSET, Y_OFFSET, null);
    }
    else {
      g2d.setColor(new Color(224,224,224));
      g2d.fillRect(X_OFFSET, Y_OFFSET, SIZE, SIZE);
    }
    paintGrid(g2d);
  }
  
  public void paintGrid(Graphics2D g2d) {
    g2d.setColor(new Color(255,255,255));
    for (int i = 1; i < SIZE_IN_TILES; i++) {
      g2d.drawLine(
        X_OFFSET, Y_OFFSET + i*TILE_SIZE, 
        X_OFFSET + SIZE, Y_OFFSET + i*TILE_SIZE
      );
      
      g2d.drawLine(
        X_OFFSET + i*TILE_SIZE, Y_OFFSET, 
        X_OFFSET + i*TILE_SIZE, Y_OFFSET + SIZE
      );
    }
  }

}
