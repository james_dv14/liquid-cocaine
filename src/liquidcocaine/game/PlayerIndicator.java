package liquidcocaine.game;

import java.awt.Color;
import java.awt.Graphics2D;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class PlayerIndicator extends Tile {
  public static final int SIZE = 3;
  PlayerToken pt;
  
  Color c;

  public PlayerIndicator(PlayerToken pt) {
    super(pt.x_tile, pt.y_tile);
    this.pt = pt;
    c = new Color(255, 255, 255);
  }
  
  public void paint(Graphics2D g2d) {
    g2d.setColor(c);
    
    int[] xPx = {
      pt.xPx() + GameMap.TILE_SIZE / 2 - SIZE - SIZE /2 + 1, 
      pt.xPx() + GameMap.TILE_SIZE / 2 - SIZE / 2 + 1,
      pt.xPx() + GameMap.TILE_SIZE / 2 + SIZE / 2 + 2
    };
    
    int[] yPx = {
      pt.yPx() + GameMap.TILE_SIZE / 2 - SIZE - SIZE /2 + 1,
      pt.yPx() + GameMap.TILE_SIZE / 2 - SIZE / 2 + 1,
      pt.yPx() + GameMap.TILE_SIZE / 2 + SIZE / 2 + 2
    };
    
    g2d.fillRect(xPx[1], yPx[0], SIZE, SIZE);
    g2d.fillRect(xPx[0], yPx[1], SIZE, SIZE);
    g2d.fillRect(xPx[1], yPx[1], SIZE, SIZE);
    g2d.fillRect(xPx[1], yPx[2], SIZE, SIZE);
    g2d.fillRect(xPx[2], yPx[1], SIZE, SIZE);
    
  }

}
