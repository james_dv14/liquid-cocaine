package liquidcocaine.game;

import java.io.Serializable;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class PlayerData implements Serializable {
  PlayerToken token;
  int lives;
  int moves_max;
  int moves_left;
}
