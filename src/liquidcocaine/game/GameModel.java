package liquidcocaine.game;

import java.awt.Color;
import java.util.HashMap;
import java.util.HashSet;
import liquidcocaine.astar.*;
import liquidcocaine.base.LiquidNode;
import liquidcocaine.client.Client;
import liquidcocaine.util.CoordinateHelper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class GameModel {
  //***************************************
  // View
  //***************************************
  MarioView view;
  //***************************************
  // Game elements
  //***************************************
  public GameMap map;
  public GameMapDimmer dimmer;
  public Goal goal;
  public HashMap<String, Obstacle> obstacles;
  public HashMap<String, Integer> player_coords;
  public HashMap<Integer, PlayerToken> player_tokens;
  public HashSet<ExploreIndicator> indicators;
  //***************************************
  // Client-only info
  //***************************************
  boolean interactive;
  int moves;
  int id;
  //***************************************
  // Constructor
  //***************************************
  public GameModel(LiquidNode node) {
    obstacles = new HashMap();
    player_coords = new HashMap();
    player_tokens = new HashMap();
    indicators = new HashSet();
    interactive = false;
    dimmer = new GameMapDimmer();
    if (node instanceof Client) id = ((Client)node).id;
  }
  //***************************************
  // Default setup
  //***************************************
  public void defaultSetup() {
    addMap();
    addGoal(GameMap.CENTER_TILE, GameMap.CENTER_TILE);
    for (int i = 0; i < GameMap.SIZE_IN_TILES; i++)
      for (int j = 0; j < GameMap.SIZE_IN_TILES; j++)
        addObstacle(i, j);
  }
  //***************************************
  // Get player coords
  //***************************************
  public String getPlayerCoords(int id) {
    String ret_coords = null;
    for (String coords : player_coords.keySet())
      if (player_coords.get(coords).equals(id)) ret_coords = coords;
    return ret_coords;
  }
  //***************************************
  // Set view
  //***************************************
  public void setView(MarioView mv) {
    view = mv;
  }
  //***************************************
  // Client receives these
  //***************************************
  public void setInteractive(boolean flag) { 
    interactive = flag; 
    if (interactive)
      removeFromView(dimmer);
  }
  public void addMoves(int m) { moves += m; }
  //***************************************
  // Add elements
  //***************************************
  public void addToView(GameObject go) {
    if (view != null) view.add(go);
  }
  
  public void addMap() {
    map = new GameMap();
    addToView(map);
  }
  
  public void addGoal(int x, int y) {
    goal = new Goal(x, y);
    addToView(goal);
  }
  
  public void addPlayer(int id, int x, int y, Color c) {
    PlayerToken pt = new PlayerToken(x, y, c);
    String coords = CoordinateHelper.getCoordString(x, y);
    player_coords.put(coords, id);
    player_tokens.put(id, pt);
    addToView(pt);
    if (id == this.id) addToView(new PlayerIndicator(pt));
  }
  
  public boolean addObstacle(int x, int y) {
    String coords = CoordinateHelper.getCoordString(x, y);
    if (isUnoccupied(coords) && !isGoalTile(coords)) {
      Obstacle obs = new Obstacle(x, y);
      obstacles.put(coords, obs);
      addToView(obs);
      return true;
    }
    else {
      return false;
    }
  }
  
  public void addExploreIndicator(int x, int y) {
    ExploreIndicator ei = new ExploreIndicator(x, y);
    indicators.add(ei);
    addToView(ei);
  }
  //***************************************
  // Perform A*
  //***************************************
  public AStarState performAStar(int id, int h) {
    Heuristic heuristic;
    switch (h % 3) {
      case 1:
        heuristic = new HeuristicManhattan();
        break;
      case 2:
        heuristic = new HeuristicEuclidean();
        break;
      default:
        heuristic = new HeuristicComposite();
    }
    
    LiquidSolver ls = new LiquidSolver(heuristic, this, id);
    return ls.getBestState();
  }
  //***************************************
  // Move player
  //***************************************
  public boolean movePlayer(int id, int x, int y) {
    String new_coords = CoordinateHelper.getCoordString(x, y);
    String remove_coords = getPlayerCoords(id);
    
    if (isUnoccupied(new_coords) && inMapBounds(new_coords)) {
      player_coords.put(new_coords, id);
      player_coords.remove(remove_coords);
      player_tokens.get(id).x_tile = x;
      player_tokens.get(id).y_tile = y;
      return true;
    }
    else {
      return false;
    }
  }
  
  public boolean displacePlayer(int id, int x, int y) {
    String old_coords = getPlayerCoords(id);
    
    String[] coords_a = old_coords.split(",");
    int old_x = Integer.parseInt(coords_a[0]);
    int old_y = Integer.parseInt(coords_a[1]);
    
    return movePlayer(id, old_x + x, old_y + y);
  }
  //***************************************
  // Remove elements
  //***************************************
  public void removeFromView(GameObject go) {
    if (view != null) view.remove(go);
  }
  
  public void removeObstacle(String coords) {
    removeFromView(obstacles.get(coords));
    obstacles.remove(coords);
  }
  
  public String getRandomObstacleCoords() {
    int i = 0;
    int rand = (int)(Math.random() * obstacles.size());
    for (String coords : obstacles.keySet()) {
      if (i == rand) return coords;
      i++;
    }
    return "";
  }
  
  public void removePlayer(Integer id) {
    removeFromView(player_tokens.get(id));
    player_tokens.remove(id);
    
    String remove_coords = getPlayerCoords(id);
    
    if (remove_coords != null)
      player_coords.remove(remove_coords);
  }
  
  public void clearIndicators() {
    for (ExploreIndicator ei : indicators)
      removeFromView(ei);
  }
  //***************************************
  // Predicate functions
  //***************************************
  public boolean isLegalMove(String coords) {
    return (isUnoccupied(coords) && inMapBounds(coords));
  }
  
  public boolean inMapBounds(String coords) {
    String[] coords_a = coords.split(",");
    for (String coord_s : coords_a) {
      int coord = Integer.parseInt(coord_s);
      if (coord < 0 || coord >= GameMap.SIZE_IN_TILES) return false;
    }
    return true;
  }
  
  public boolean isUnoccupied(String coords) {
    if (obstacles.get(coords) != null) return false;
    if (player_coords.get(coords) != null) return false;
    return true;
  }
  
  public boolean isGoalTile(String coords) {
    String goal_coords = 
      CoordinateHelper.getCoordString(goal.x_tile, goal.y_tile);
    
    return goal_coords.equals(coords);
  }
  
}