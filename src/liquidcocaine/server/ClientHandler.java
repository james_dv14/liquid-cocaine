package liquidcocaine.server;

import liquidcocaine.base.*;
import liquidcocaine.net.*;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ClientHandler {
  //***************************************
  // Objects
  //***************************************
  private final NetConnection sender;
  private final Receiver receiver;
  //***************************************
  // Constructor
  //***************************************
  public ClientHandler(Server s, NetConnection c) {
    sender = c;
    receiver = new Receiver((LiquidNode)s, c);
  }
  //***************************************
  // Getters
  //***************************************
  public NetConnection getSender() { return sender; }
  public Receiver getReceiver() { return receiver; }
  //***************************************
  // Thread start
  //***************************************
  public void start(int id) {
    Thread t = new Thread(receiver);
    t.setName("Receiver (server to Player" + id + ")");
    t.start();
  }
  //***************************************
  // Teardown
  //***************************************
  public void teardown() {
    receiver.teardown();
  }
}
