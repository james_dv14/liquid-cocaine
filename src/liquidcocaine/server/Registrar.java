package liquidcocaine.server;

import liquidcocaine.base.*;
import liquidcocaine.client.ClientDatagram;
import liquidcocaine.net.*;
import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import liquidcocaine.base.LiquidModule;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Registrar extends LiquidModule {
  //***************************************
  // Constants
  //***************************************
  public static final String MODULE_NAME = "Server Registrar";
  //***************************************
  // Objects
  //***************************************
  private int id;
  private final Server server;
  //***************************************
  // Constructor
  //***************************************
  public Registrar(Server s) {
    super((LiquidNode) s);
    id = 1;
    server = s;
    good = true;
  }
  //***************************************
  // Main method
  //***************************************
  public void registerClient(Socket s) {
    NetConnection c = new NetConnection(s);
    ClientHandler ch = new ClientHandler(server, c);
    ch.start(id);
    System.out.println("S: Player" + id + " connected!");
    server.clients.put(id, ch);
    ch.getSender().sendObject(new ClientDatagram.SetID(id));
    id++;
  }
  //***************************************
  // Implementations
  //***************************************
  @Override
  public void loop() {
    try {
      Socket s = server.ss.accept();
      if (good)
        registerClient(s);
      else
        s.close();
    } 
    catch (IOException ex) {
      ex.printStackTrace();
      teardown();
    }
  }
  
  @Override
  public void teardown() {
    super.teardown();
    try {
      Socket s = new Socket("127.0.0.1", server.port);
    } 
    catch (IOException ex) {
      Logger.getLogger(Registrar.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}
