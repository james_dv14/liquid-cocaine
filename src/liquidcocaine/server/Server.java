package liquidcocaine.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import liquidcocaine.base.*;
import liquidcocaine.client.ClientDatagram;
import liquidcocaine.net.Protocol;
import liquidcocaine.util.*;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Server extends LiquidNode {
  
  HashMap<Integer, ClientHandler> clients;
  
  Registrar reg;
  ServerSocket ss;
  //***************************************
  // Constructor
  //***************************************
  public Server(int port) {
    super();
    this.port = port;
    clients = new HashMap();
  }
  //***************************************
  // Main
  //***************************************
  public static void main(String args[]) {
    String arg = args.length > 0 ? args[0] : "";
    
    Server server = new Server(PortHelper.extractPort(arg));
    
    String name = "Server";
    
    ThreadHelper.start(server, name);
    
    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        server.teardown();
      }
    });
  }
  //***************************************
  // Run
  //***************************************
  @Override
  public void run() {
    try {
      ss = new ServerSocket(port);
      reg = new Registrar(this);
      
      // wait for required clients
      while (clients.size() < 1) {
        String msg = "Waiting for player(s)...";
        System.out.println("S: " + msg);
        Socket s = ss.accept();
        reg.registerClient(s);
      }
    
      // wait for more clients
      System.out.print("S: Press any key to start the game, ");
      System.out.println("or wait for more players to connect.");

      addModule(reg, Registrar.MODULE_NAME);

      String wala = Helper.promptMessage("");
      removeModule(Registrar.MODULE_NAME);
      
      // start game
      addModule(new ServerGame(this), ServerGame.MODULE_NAME);
    } 
    catch (IOException ex) {
      System.out.println("S: " + Protocol.ERROR);
      ex.printStackTrace();
      System.exit(0);
    }
  }
  //***************************************
  // Implementations
  //***************************************
  @Override
  public void teardown() {
    sendToAllClients(new ClientDatagram.Quit(Protocol.SERVER_ID));
  }
  //***************************************
  // Utility
  //***************************************
  public void sendToAllClients(Datagram datagram) {
    for (ClientHandler ch : clients.values())
      ch.getSender().sendObject(datagram);
  }
  
  public void sendToAllClientsExcept(Datagram datagram, int id) {
    for (int _id : clients.keySet())
      if (_id != id) clients.get(_id).getSender().sendObject(datagram);
  }
    
}