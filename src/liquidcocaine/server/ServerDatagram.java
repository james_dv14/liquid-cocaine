package liquidcocaine.server;

import liquidcocaine.astar.AStarState;
import liquidcocaine.base.*;
import liquidcocaine.client.ClientDatagram;
import liquidcocaine.net.Protocol;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ServerDatagram {
  //***************************************
  // Connection datagrams
  //***************************************
  public static class Message implements Datagram {
    int sender_id;
    String message;
    
    public Message(int id, String m) {
      sender_id = id;
      message = m;
    }
    
    @Override
    public void execute(LiquidNode ln) {
      Server server = (Server)ln;
      server.sendToAllClients(new ClientDatagram.Message(sender_id, message));
    }
  }
  
  public static class Quit implements Datagram {
    int sender_id;
    
    public Quit(int id) {
      sender_id = id;
    }
    
    @Override
    public void execute(LiquidNode ln) {
      Server server = (Server)ln;
      System.out.println("Player" + sender_id + " quit.");
      
      server.sendToAllClients(new ClientDatagram.Quit(sender_id));
      
      LiquidModule gm = server.modules.get(ServerGame.MODULE_NAME);
      if (gm != null)
        ((ServerGame)gm).model.removePlayer(sender_id);
      
      server.clients.get(sender_id).teardown();
      server.clients.remove(sender_id);
      
      if (server.clients.size() == 1) {
        for (int id : server.clients.keySet())
          server.clients.get(id).getSender().sendObject(
            new ClientDatagram.DeclareWinner(id)
          );
      }
      else if (server.clients.size() == 1) {
        server.teardown();
      }
    }
  }
  //***************************************
  // Game datagrams
  //***************************************
  public abstract class ServerGameDatagram implements Datagram {
    Server server;
    ServerGame game;
    
    public void execute(LiquidNode ln) {
      server = (Server)ln;
      game = (ServerGame)(ln.modules.get(ServerGame.MODULE_NAME));
      _execute();
    }
    
    public abstract void _execute();
  }
  
  public static class Ack extends ServerGameDatagram {
    int sender_id;
    
    public Ack(int id) {
      sender_id = id;
    }
    
    @Override
    public void _execute() {
      game.ack.add(sender_id);
    }
  }
  
  public static class DisplacePlayer extends ServerGameDatagram {
    int sender_id;
    int x;
    int y;
    
    public DisplacePlayer(int id, int x, int y) {
      sender_id = id;
      this.x = x;
      this.y = y;
    }
    
    @Override
    public void _execute() {
      game.displacePlayer(sender_id, x, y);
    }
  }
  
  
  public static class ToggleLock extends ServerGameDatagram {
    @Override
    public void _execute() {
      game.unlocked = !game.unlocked;
    }
  }
  
  public static class PerformAStar extends ServerGameDatagram { 
    int sender_id;
    int h;
    
    public PerformAStar(int id, int h) {
      sender_id = id;
      this.h = h;
    }
    
    @Override
    public void _execute() {
      game.unlocked = false;
      game.sendAndWait(new ClientDatagram.SetInteractive(false), 1000, 1000);
      game.sendAndWait(new ClientDatagram.PerformAStar(sender_id, h), 250, 50);
      
      AStarState best_state = game.model.performAStar(sender_id, h);
      if (best_state.h == 0) {
        server.sendToAllClients(new ClientDatagram.DeclareWinner(sender_id));
      }
      else {
        game.sendAndWait(sender_id, new ClientDatagram.Lose(), 1000, 1000);
        game.unlocked = true;
        game.sendAndWait(new ClientDatagram.SetInteractive(true), 250, 50);
      }
    }
  }
  
}