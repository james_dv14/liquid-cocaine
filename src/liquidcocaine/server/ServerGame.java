package liquidcocaine.server;

import java.awt.Color;
import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import liquidcocaine.base.*;
import liquidcocaine.game.*;
import liquidcocaine.util.*;
import liquidcocaine.client.ClientDatagram;
import liquidcocaine.net.Protocol;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ServerGame extends LiquidModule {
  //***************************************
  // Constants
  //***************************************
  public static final String MODULE_NAME = "Server Game";
  //***************************************
  // Objects
  //***************************************
  Server server;
  GameModel model;
  HashSet<Integer> ack;
  boolean unlocked;
  //***************************************
  // Constructor
  //***************************************
  public ServerGame(LiquidNode node) {
    super(node);
    server = (Server)node;
  }
  //***************************************
  // Implementations
  //***************************************
  @Override
  public void setup() {
    server.sendToAllClients(new ClientDatagram.Message(
      Protocol.SERVER_ID, "Time to play the game."
    ));
    
    model = new GameModel(server);
    model.defaultSetup();
    
    sendAndWait(new ClientDatagram.StartGame(), 1000, 250);
    sendAndWait(new ClientDatagram.SetInteractive(false), 250, 50);
    
    setUpPlayerTokens();
    
    sendAndWait(new ClientDatagram.AddMoves(1), 2500, 500);
    sendAndWait(new ClientDatagram.SetInteractive(true), 250, 50);
    unlocked = true;
  }
  
  @Override
  public void loop() {
    try {
      Thread.sleep(250);
    } 
    catch (InterruptedException ex) {
      Logger.getLogger(ServerGame.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    if (!model.obstacles.isEmpty() && unlocked) {
      int change_count = (int)(Math.pow(GameMap.SIZE_IN_TILES,2) / 8);
      int remove_count = change_count + (int)(Math.random() * change_count / 2);
      removeObstacles(remove_count);
      addObstacles((int)(Math.random() * change_count) / 2);
    }
  }
  //***************************************
  // Subroutines
  //***************************************
  private void setUpPlayerTokens() {
    int i = 0;
    double increment = 360.0 / server.clients.size();
    
    for (int id : server.clients.keySet()) {
      double angle = Math.toRadians(90.0 + increment * i);
      int center = GameMap.CENTER_TILE;
      
      int x = center - (int)Math.round(center * Math.cos(angle));
      int y = center - (int)Math.round(center * Math.sin(angle));
      Color c = ColorHelper.getRandomColor(127, 0);
      c = new Color(c.getRed(), c.getGreen(), c.getBlue(), 255);
      
      String obs_coords = CoordinateHelper.getCoordString(x, y);
      model.removeObstacle(obs_coords);
      sendAndWait(new ClientDatagram.RemoveObstacle(obs_coords), 50, 5);
      
      model.addPlayer(id, x, y, c);
      sendAndWait(new ClientDatagram.AddPlayer(id, x, y, c), 250, 50);
      
      i++;
    }
  }
  
  private void addObstacles(int count) {
    // loop control
    int done = 0;
    int tries = 0;
    // stop conditions
    int max_tries = 1000;
    // loop
    while(done < count && tries < max_tries) {
      int x = (int)(Math.random() * GameMap.SIZE_IN_TILES);
      int y = (int)(Math.random() * GameMap.SIZE_IN_TILES);
      
      if (model.addObstacle(x, y)) {
        if (unlocked) sendAndWait(new ClientDatagram.AddObstacle(x, y), 50, 5);
        done++;
      }
      
      tries++;
    }
  }
  
  private void removeObstacles(int count) {
    // loop control
    int done = 0;
    String coords;
    // loop
    do {
      coords = model.getRandomObstacleCoords();
      model.removeObstacle(coords);
      if (unlocked) sendAndWait(new ClientDatagram.RemoveObstacle(coords), 50, 5);
      done++;
    } while (done < count && !coords.isEmpty());
  }
  
  void displacePlayer(int id, int x, int y) {
    if (model.displacePlayer(id, x, y)) {
      // sendAndWait(id, new ClientDatagram.AddMoves(-1), 50, 25);
      sendAndWait(new ClientDatagram.DisplacePlayer(id, x, y), 100, 25);
      
      if (model.isGoalTile(model.getPlayerCoords(id))) {
        sendAndWait(new ClientDatagram.SetInteractive(false), 250, 50);
        server.sendToAllClients(new ClientDatagram.DeclareWinner(id));
      } 
    }
  }
  //***************************************
  // Utility
  //***************************************
  
  /**
   * Sends a datagram to all clients and waits for their ACK datagrams.
   * @param datagram The datagram.
   * @param max The maximum time to wait, in milliseconds.
   * @param step The time interval for counting ACKs, in milliseconds.
   */
  void sendAndWait(Datagram datagram, int max, int step) {
    ack = new HashSet();
    server.sendToAllClients(datagram);
    waitForAcks(max, step);
  }
  
  
  /**
   * Sends a datagram to all but one client and waits their ACK datagrams.
   * @param id The client's id.
   * @param datagram The datagram.
   * @param max The maximum time to wait, in milliseconds.
   * @param step The time interval for counting ACKs, in milliseconds.
   */
  void sendToAllExceptAndWait(Datagram datagram, int id, int max, int step) {
    ack = new HashSet();
    ack.add(-1);
    server.sendToAllClientsExcept(datagram, id);
    waitForAcks(max, step);
  }
  
  /**
   * Sends a datagram to one client and waits for his/her ACK datagram.
   * @param id The client's id.
   * @param datagram The datagram.
   * @param max The maximum time to wait, in milliseconds.
   * @param step The time interval for counting ACKs, in milliseconds.
   */
  void sendAndWait(int id, Datagram datagram, int max, int step) {
    ack = new HashSet();
    server.clients.get(id).getSender().sendObject(datagram);
    waitForAck(id, max, step);
  }
  
  /**
   * Checks if a particular client has sent an ACK datagram for a certain 
   * action.
   * @param id The client's id.
   * @param max The maximum time to wait, in milliseconds.
   * @param step The time interval for counting ACKs, in milliseconds.
   */
  void waitForAck(int id, int max, int step) {
    int elapsed = 0;
    while (!ack.contains(id) && elapsed < max) {
      try {
        Thread.sleep(step);
        elapsed += step;
      } 
      catch (InterruptedException ex) {
        Logger.getLogger(ServerGame.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }
  
  /**
   * Checks if each client has sent ACK datagrams for a certain action.
   * @param max The maximum time to wait, in milliseconds.
   * @param step The time interval for counting ACKs, in milliseconds.
   */
  void waitForAcks(int max, int step) {
    int elapsed = 0;
    while (ack.size() < server.clients.size() && elapsed < max) {
      try {
        Thread.sleep(step);
        elapsed += step;
      } 
      catch (InterruptedException ex) {
        Logger.getLogger(ServerGame.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
  }

}
