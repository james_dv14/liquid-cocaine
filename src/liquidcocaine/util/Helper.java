package liquidcocaine.util;

import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Helper {
  //***************************************
  // Prompts
  //***************************************

  /**
   * Returns console input the user provides in response to a given prompt.
   * @param prompt The prompt.
   * @return The console input as a string.
   */
  public static String promptMessage(String prompt) {
    String msg;
    
    if (prompt != null) 
      if (!prompt.isEmpty())
        System.out.print(prompt);
    
    BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
    try {
      msg = input.readLine();
    }
    catch (IOException e) {
      msg = "";
    }
    return msg;
  }

  //***************************************
  // String sanitation
  //***************************************

  public static String removeLeadingWhitespace(String str) {
    return str.replaceFirst("\\s+$", "");
  }

  public static String getFirstWord(String str) {
    return str.contains(" ") ? str.substring(0, str.indexOf(" ")) : str;
  }

  //***************************************
  // Math
  //***************************************

  /**
   * Gets the positive result of a modulus operation, even with a negative
   * dividend.
   * @param a The dividend.
   * @param b The divisor.
   * @return The positive result of the modulus operation.
   */
  public static int getPositiveMod (int a, int b) {
    return (a % b + b) % b;
  }

  //***************************************
  // Date and Time
  //***************************************

  /**
   * Returns a timestamp of the current date/time in the specified format.
   * @param format A format for SimpleDateFormat.
   * @return A timestamp of the current date/time in the specified format.
   */
  public static String getTimeStamp(String format) {
    return new SimpleDateFormat(format).format(new Date());
  }
}
