package liquidcocaine.util;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class CoordinateHelper {

  /**
   * Convert a coordinate pair into a string for use as a hash key.
   * @param x The x-coordinate.
   * @param y The y-coordinate.
   * @return An string of the format x,y.
   */
  public static String getCoordString(int x, int y) {
    return x + "," + y;
  }
  
    /**
   * Convert a coordinate string into an integer array for use in A*.
   * @param coord_str A string in format x,y.
   * @return An array in the form {x,y}
   */
  public static int[] getCoordArray(String coord_str) {
    String[] coord_arr = coord_str.split(",");
    int[] ret_arr = new int[coord_arr.length];
    for (int i = 0; i < coord_arr.length; i++)
      ret_arr[i] = Integer.parseInt(coord_arr[i]);
    return ret_arr;
  }
}
