package liquidcocaine.util;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class PortHelper {
  
  /**
   * Extracts a port number from a command-line argument. Returns a default 
   * value if no valid port can be extracted.
   * @param arg The argument.
   * @return The port number, as an integer.
   */
  public static int extractPort(String arg) {
    int port_t;
    try {
      String port_str = arg.substring(arg.indexOf(":") + 1, arg.length());
      if (!isValidPort(port_str))
        throw new Exception();
      else
        port_t = Integer.parseInt(port_str);
    }
    catch (Exception e) {
      port_t = 8888;
    }
    return port_t;
  }

  /**
   * Checks if a given string is a valid port number (0-65535).
   * @param port The candidate port number, as a string.
   * @return  True if the string is a valid port number, false otherwise.
   */
  public static boolean isValidPort(String port) {
    try {
      // Check if string is null or empty.
      if (port == null || port.isEmpty()) return false;
      // Check if the string is an integer from 0-65535.
      int i = Integer.parseInt(port);
      return !(i < 0 || i > 65535);
    } 
    catch (NumberFormatException nfe) {
      return false;
    }
  }

}
