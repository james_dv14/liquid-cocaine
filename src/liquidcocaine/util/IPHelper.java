package liquidcocaine.util;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class IPHelper {
  
  /**
   * Extracts an IPv4 address from a command-line argument. Returns a default 
   * value if no valid IPv4 address can be extracted.
   * @param arg The argument.
   * @return The IPv4 address, as a string.
   */
  public static String extractIPv4(String arg) {
    String ip;
    try {
      ip = arg.substring(0, arg.indexOf(":"));
      if (!isValidIPv4(ip))
        throw new Exception();
    }
    catch (Exception e) {
      ip = "127.0.0.1";
    }
    return ip;
  }

  /**
   * Checks if a given string is a valid IPv4 address
   * @credit StackOverflow user rouble
   * @source "Validating IPv4 string in Java". StackOverflow.
   * @url http://stackoverflow.com/questions/4581877/validating-ipv4-string-in-java
   * @param ip The candidate IP address, as a string.
   * @return True if the string is a valid IPv4 address, false otherwise.
   */
  public static boolean isValidIPv4(String ip) {
    try {
      // Check if string is null or empty.
      if (ip == null || ip.isEmpty()) return false;
      // Check if string has 4 parts.
      String[] parts = ip.split("\\.", -1);
      if ( parts.length != 4 ) return false;
      // Check if each part is an integer from 0-255.
      for ( String s : parts ) {
        int i = Integer.parseInt( s );
        if (i < 0 || i > 255) return false;
      }
      return true;
    } 
    catch (NumberFormatException nfe) {
      return false;
    }
  }

}
