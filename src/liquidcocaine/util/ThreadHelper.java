package liquidcocaine.util;

import java.util.Set;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ThreadHelper {
  
  /**
   * Runs a Runnable in a thread.
   * @param r A Runnable.
   * @param name A name for the thread.
   */
  public static void start(Runnable r, String name) {
    Thread t = new Thread(r);
    t.setName(name);
    t.start();
  }
  
  /**
   * Prints information on all currently running threads to the console.
   */
  public static void listThreads() {
    Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
    for (Thread thread : threadSet) {
      System.out.print(thread.toString());
      System.out.print(" ");
      System.out.println(thread.getClass().toString());
    }
  }

}
