package liquidcocaine.util;

import java.awt.Color;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ColorHelper {
  
  /**
   * Returns a Color with random rgb values from 0-255.
   * @return The Color.
   */
  public static Color getRandomColor() {
    return new Color(
      (int)(Math.random() * 255),
      (int)(Math.random() * 255),
      (int)(Math.random() * 255)
    );
  }
  
  /**
   * Returns a Color with random rgb values in a specified range.
   * @param max The maximum.
   * @param min The minimum.
   * @return The Color.
   */
  public static Color getRandomColor(int max, int min) {
    min = Math.max(min, 0);
    max = Math.min(max, 255);
    
    return new Color(
      min + (int)(Math.random() * max-min),
      min + (int)(Math.random() * max-min),
      min + (int)(Math.random() * max-min)
    );
  }

}
