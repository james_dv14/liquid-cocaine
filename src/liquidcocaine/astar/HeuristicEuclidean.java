package liquidcocaine.astar;

import liquidcocaine.util.CoordinateHelper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class HeuristicEuclidean extends Heuristic {
  
  public HeuristicEuclidean() {
    name = "Euclidean distance";
  }
  
  @Override
  public double getDistance(String current, String goal) {
    int[] coord_arr = CoordinateHelper.getCoordArray(current);
    int current_x = coord_arr[0];
    int current_y = coord_arr[1];
    
    int[] goal_arr = CoordinateHelper.getCoordArray(goal);
    int goal_x = goal_arr[0];
    int goal_y = goal_arr[1];
    
    double result = Math.pow(goal_x - current_x, 2);
    result += Math.pow(goal_y - current_y, 2);
    return Math.sqrt(result);
  }

}
