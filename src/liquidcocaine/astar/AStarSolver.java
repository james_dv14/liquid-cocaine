package liquidcocaine.astar;

import java.util.HashSet;
import java.util.PriorityQueue;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public abstract class AStarSolver {
  public AStarState start_state;
  public AStarState best_state;
  
  long startTime;
  long endTime;
    
  public AStarSolver(AStarState s) {
    start_state = s;
    best_state = s;
  }
    
  public AStarState getBestState() {
    setup();
    startTime = System.currentTimeMillis();
    
    HashSet<String> closed_set = new HashSet();
    PriorityQueue<AStarState> open_set = new PriorityQueue();
    open_set.add(start_state);
    
    while (best_state.h > 0 && !open_set.isEmpty()) {
      AStarState closest_child = open_set.poll();
      closed_set.add(closest_child.value);
      processNode(closest_child);
      
      closest_child.generateChildren();
      for (AStarState child : closest_child.children) {
        if (!closed_set.contains(child.value)) {
          if (child.h < best_state.h)
            best_state = child;
          open_set.add(child);
          closed_set.add(child.value);
        }
      }
    }
            
    endTime = System.currentTimeMillis();
    cleanup();
    return best_state;
  }
  
  protected abstract void processNode(AStarState node);
  protected abstract void setup();
  protected abstract void cleanup();
    
  public long getTimeElapsed() {
    return endTime - startTime;
  }        
}
