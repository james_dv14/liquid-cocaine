package liquidcocaine.astar;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class HeuristicEvaluator {
  //***************************************
  // Fields
  //***************************************
  public String goal;
  public Heuristic h;
  //***************************************
  // Constructor
  //***************************************
  public HeuristicEvaluator(String goal, Heuristic h) {
    this.goal = goal;
    this.h = h;
  }
  //***************************************
  // Method
  //***************************************
  public double getDistance(String current) {
    return h.getDistance(current, goal);
  }
}
