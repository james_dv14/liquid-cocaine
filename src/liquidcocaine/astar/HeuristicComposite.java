package liquidcocaine.astar;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class HeuristicComposite extends Heuristic {
  
  public HeuristicComposite() {
    name = "Euclidean + Manhattan distance";
  }
  
  @Override
  public double getDistance(String current, String goal) {
    double distance = new HeuristicEuclidean().getDistance(current, goal);
    distance += new HeuristicManhattan().getDistance(current, goal);
    return distance;
  }

}
