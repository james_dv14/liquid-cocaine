package liquidcocaine.astar;

import java.util.HashSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import liquidcocaine.game.*;
import liquidcocaine.util.CoordinateHelper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class LiquidSolver extends AStarSolver {
  Heuristic h;
  public GameModel model;
  HashSet<GameObject> indicators;
  
  public LiquidSolver(Heuristic h, GameModel model, int id) {
    super(new LiquidState(
      new HeuristicEvaluator("9,9", h), model.getPlayerCoords(id), model
    ));
    this.h = h;
    this.model = model;
    indicators = new HashSet();
  }
  
  @Override
  protected void processNode(AStarState node) {
    indicators = new HashSet();
    int[] coords = CoordinateHelper.getCoordArray(node.value);
    model.addExploreIndicator(coords[0], coords[1]);
  }

  @Override
  protected void setup() {
    model.clearIndicators();
  }

  @Override
  protected void cleanup() {
    System.out.println("Heuristic: " + h.name);
    System.out.println("Running time: " + getTimeElapsed() + "ms");
    System.out.println("Result tile: " + best_state.value);
  }

}
