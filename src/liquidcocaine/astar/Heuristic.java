package liquidcocaine.astar;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public abstract class Heuristic {
  String name;
  
  public abstract double getDistance(String current, String goal);
}