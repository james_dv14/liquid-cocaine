package liquidcocaine.astar;

import java.util.ArrayList;
import java.util.HashSet;
import liquidcocaine.game.GameModel;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class LiquidState extends AStarState {
  //***************************************
  // Fields
  //***************************************
  public GameModel model;
  public ArrayList<String> move_path;
  //***************************************
  // Public (root) constructor
  //***************************************
  public LiquidState(HeuristicEvaluator he, String value, GameModel model) {
    super(he, value);
    this.model = model;
    move_path = new ArrayList();
  }
  //***************************************
  // Protected (child) constructor
  //***************************************
  protected LiquidState (AStarState parent, String value, String move) {
    super(parent, value);
    model = ((LiquidState)parent).model;
    move_path = new ArrayList(((LiquidState)parent).move_path);
    move_path.add(move);
  }
  //***************************************
  // Move
  //***************************************
  String displace(int x, int y) {
    String[] coord_arr = value.split(",");
    if (x != 0) coord_arr[0] = "" + (Integer.parseInt(coord_arr[0]) + x);
    if (y != 0) coord_arr[1] = "" + (Integer.parseInt(coord_arr[1]) + y);
    return coord_arr[0] + "," + coord_arr[1];
  }
    
  String moveUp() { return displace(0, -1); }
  String moveDown() { return displace(0, 1); }
  String moveLeft() { return displace(-1, 0); }
  String moveRight() { return displace(1, 0); }
  //***************************************
  // Sex function
  //***************************************
  @Override
  public void generateChildren() {
    children = new HashSet();

    String child_val;
    String move;
    
    for (int i = 0; i < 4; i++) {
      switch (i) {
        case 0:
          child_val = moveUp();
          move = "UP";
          break;
        case 1:
          child_val = moveDown();
          move = "DOWN";
          break;
        case 2:
          child_val = moveLeft();
          move = "LEFT";
          break;
        case 3:
          child_val = moveRight();
          move = "RIGHT";
          break;
        default:
          child_val = null;
          move = null;
      }
      
      if (child_val == null)
        continue;
      else if (!model.isLegalMove(child_val))
         continue;
      else if (parent != null)
        if (parent.value.equals(child_val)) continue;
      
      LiquidState child = new LiquidState(this, child_val, move);
      children.add(child);
    }
  }
}
