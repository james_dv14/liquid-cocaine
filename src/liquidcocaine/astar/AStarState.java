package liquidcocaine.astar;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */

import java.util.HashSet;
import java.util.Stack;

public abstract class AStarState implements Comparable {
  //***************************************
  // Fields
  //***************************************
  public String value;
  public HeuristicEvaluator he;
    
  public int g;           // cost in steps from start state
  public double h;        // distance from goal state according to heuristic
  //***************************************
  // Objects
  //***************************************
  public HashSet<AStarState> children;
  public AStarState parent;
  //***************************************
  // Public (root) constructor
  //***************************************
  public AStarState (HeuristicEvaluator he, String value) {
    this.he = he;
    this.value = value;

    h = he.getDistance(value);
    g = 0;
  }
  //***************************************
  // Protected (child) constructor
  //***************************************
  protected AStarState (AStarState parent, String value) {
    this(parent.he, value);
    this.parent = parent;
    
    g = parent.g + 1;
  }
  //***************************************
  // Abstract method
  //***************************************
  public abstract void generateChildren();
  //***************************************
  // Path retrieval
  //***************************************
  public Stack<String> getPathStack() {
    Stack<String> path_stack = new Stack();
    AStarState node = this;
    while (node.parent != null) {
      path_stack.push(processValue(node.value));
      node = node.parent;
    }
    return path_stack;
  }
  
  public String processValue(String value) {
    return value;
  }
  //***************************************
  // Natural comparison
  //***************************************
  @Override
  public int compareTo(Object o) {
    AStarState other = (AStarState) o;
    double total = (double)g + h;
    double other_total = (double)other.g + other.h;
    return Double.compare(total, other_total);
  }
}