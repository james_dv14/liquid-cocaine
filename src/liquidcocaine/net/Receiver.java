package liquidcocaine.net;

import liquidcocaine.base.*;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Receiver extends LiquidModule {
  //***************************************
  // Constants
  //***************************************
  public static final String MODULE_NAME = "Receiver";
  //***************************************
  // Objects
  //***************************************
  private final NetConnection connection;
 //***************************************
  // Constructor
  //***************************************
  public Receiver(LiquidNode ln, NetConnection c){
    super(ln);
    connection = c;
    good = true;
  }
  //***************************************
  // Run
  //***************************************
  public void loop() {
    try {
      Datagram d = (Datagram)connection.receiveObject();
      if (good) node.receiveDatagram(d);
    }
    catch (Exception e) {
      System.out.println("S: " + Protocol.ERROR);
      e.printStackTrace();
      teardown();
    }
  }
}
