package liquidcocaine.net;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */

import java.io.*;
import java.net.*;

public class NetConnection {
  //***************************************
  // Fields
  //***************************************
  protected Socket s; 
  private final ObjectOutputStream out;
  private final ObjectInputStream in;
  //***************************************
  // Constructor
  //***************************************
  public NetConnection(Socket s) {
    this.s = s;
    out = getOutputStream();
    in = getInputStream();
  }
  //***************************************
  // Send / receive
  //***************************************
  public boolean sendObject(Object o) {
    try {
      out.writeObject(o);
      return true;
    }	
    catch (Exception e) {
      return false;
    }	
  }

  public Object receiveObject() {
    try {
      return in.readObject();
    }
    catch (IOException | ClassNotFoundException e) {
      return null;
    }
  }
  //***************************************
  // Initialize I/O streams
  //***************************************
  private ObjectOutputStream getOutputStream() {
    try {
      return new ObjectOutputStream(s.getOutputStream());
    }
    catch (IOException e) {
      System.out.print("ERROR: Cannot extract output stream.");
      e.printStackTrace();
      return null;
    }
  }
  
  private ObjectInputStream getInputStream() {
    try {
      return new ObjectInputStream(s.getInputStream());
    }
    catch (IOException e) {
      System.out.print("ERROR: Cannot extract input stream.");
      e.printStackTrace();
      return null;
    }
  }
}
