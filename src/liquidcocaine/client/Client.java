package liquidcocaine.client;

import java.io.IOException;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import liquidcocaine.base.*;
import liquidcocaine.net.*;
import liquidcocaine.server.ServerDatagram;
import liquidcocaine.util.*;

/** 
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Client extends LiquidNode {
  //***************************************
  // Package-visible fields
  //***************************************
  public int id;
  String ip;
  NetConnection connection;
  Socket socket;
  Receiver receiver;
  //***************************************
  // Constructor
  //***************************************
  public Client(String ip, int port) {
    super();
    this.ip = ip;
    this.port = port;
  }
  //***************************************
  // Main method
  //***************************************
  public static void main(String args[]) {
    String arg = args.length > 0 ? args[0] : "";
    
    Client client = new Client(
      IPHelper.extractIPv4(arg),
      PortHelper.extractPort(arg)
    );
    
    String name = "Client";
    ThreadHelper.start(client, name);
    
    Runtime.getRuntime().addShutdownHook(new Thread() {
      @Override
      public void run() {
        client.teardown();
      }
    });
  }
  //***************************************
  // Send
  //***************************************  
  public void send(Object o) {
    connection.sendObject(o);
  }
  //***************************************
  // Implementations
  //***************************************
  @Override
  public void run() {
    try {
      System.out.println("C: Connecting to server...");
      socket = new Socket(ip, port);
      connection = new NetConnection(socket);
      System.out.println("C: Connected.");
      
      addModule(new Receiver(this, connection), "Client" + Receiver.MODULE_NAME);

      addModule(new Sender(this), Sender.MODULE_NAME);
    }
    catch (Exception e) {
      System.out.println("C: " + Protocol.ERROR);
      e.printStackTrace();
      teardown();
    }
  }
  
  @Override
  public void teardown() {
    super.teardown();
    try {
      connection.sendObject(new ServerDatagram.Quit(id));
      socket.close();
    } 
    catch (IOException ex) {
      Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}