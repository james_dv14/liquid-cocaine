package liquidcocaine.client;

import java.util.HashMap;
import liquidcocaine.base.*;
import liquidcocaine.game.*;
import liquidcocaine.server.ServerDatagram;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ClientGame extends LiquidModule {
  //***************************************
  // Constants
  //***************************************
  public static final String MODULE_NAME = "Client Game";
  //***************************************
  // Objects
  //***************************************
  GameModel model;
  MarioView view;
  //***************************************
  // Constructor
  //***************************************
  public ClientGame(LiquidNode node) {
    super(node);
  }
  //***************************************
  // Implementations
  //***************************************
  @Override
  public void setup() {
    model = new GameModel(node);
    view = new MarioView(node);

    model.setView(view);
    view.setModel(model);

    model.defaultSetup();

    view.display();
    
    Client client = (Client)node;
    client.connection.sendObject(new ServerDatagram.Ack(client.id));
  }

  @Override
  public void loop() {
    
  }

}
