package liquidcocaine.client;

import liquidcocaine.base.*;
import liquidcocaine.server.ServerDatagram;
import liquidcocaine.util.Helper;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class Sender extends LiquidModule {
  //***************************************
  // Constants
  //***************************************
  public static final String MODULE_NAME = "Client Sender";
  //***************************************
  // Constructor
  //***************************************
  public Sender(LiquidNode node) {
    super(node);
  }
  //***************************************
  // Main method
  //***************************************
  public void loop() {
    Client client = (Client)node;
    String line = Helper.promptMessage("");
    if (!client.connection.sendObject(new ServerDatagram.Message(client.id, line)))
      teardown();
  }
}