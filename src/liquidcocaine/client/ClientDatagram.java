package liquidcocaine.client;

import java.awt.Color;
import javax.swing.JOptionPane;
import liquidcocaine.base.*;
import liquidcocaine.net.*;
import liquidcocaine.server.ServerDatagram;

/**
 *
 * @author MARIANO, J Stephen DV
 * @studentno 2012-78002
 */
public class ClientDatagram {
  public static boolean verbose = false;
  //***************************************
  // Connection datagrams
  //***************************************
  public static class Message implements Datagram {
    int sender_id;
    String message;
    
    public Message(int id, String m) {
      sender_id = id;
      message = m;
    }
    
    @Override
    public void execute(LiquidNode ln) {
      if (verbose) System.out.println("P" + sender_id + ": " + message);
    }
  }
  
  public static class SetID implements Datagram {
    int new_id;
    
    public SetID(int id) {
      new_id = id;
    }
    
    @Override
    public void execute(LiquidNode ln) {
      Client client = (Client)ln;
      client.id = new_id;
    }
  }
  
  public static class Quit implements Datagram {
    int sender_id;
    
    public Quit(int id) {
      sender_id = id;
    }
    
    @Override
    public void execute(LiquidNode ln) {
      if (sender_id == Protocol.SERVER_ID) {
        if (verbose) System.out.println("Server quit.");
        ln.teardown();
      }
      else {
        if (verbose) System.out.println("Player" + sender_id + " quit.");
        
        LiquidModule gm = ln.modules.get(ClientGame.MODULE_NAME);
        if (gm != null) {
          ClientGame game = (ClientGame)gm;
          game.model.clearIndicators();
          game.model.removePlayer(sender_id);
        }
      }
    }
  }
  //***************************************
  // Game datagrams
  //***************************************
  public abstract class ClientGameDatagram implements Datagram {
    Client client;
    ClientGame game;
    
    public void execute(LiquidNode ln) {
      client = (Client)ln;
      game = (ClientGame)(ln.modules.get(ClientGame.MODULE_NAME));
      _execute();
    }
    
    public abstract void _execute();
  }
  
  public static class StartGame implements Datagram {
    @Override
    public void execute(LiquidNode ln) {
      ln.addModule(new ClientGame(ln), ClientGame.MODULE_NAME);
    }
  }
  
  public static class SetInteractive extends ClientGameDatagram {
    boolean interactive;
    
    public SetInteractive(boolean i) {
      interactive = i;
    }
    
    @Override
    public void _execute() {
      if (verbose) 
        System.out.println("Received SetInteractive(" + interactive + ").");
      game.model.setInteractive(interactive);
    }
  }
  
  public static class AddMoves extends ClientGameDatagram {
    int moves;
    
    public AddMoves(int m) {
      moves = m;
    }
    
    @Override
    public void _execute() {
      if (verbose) System.out.println("Received AddMoves(" + moves + ").");
      game.model.addMoves(moves);
    }
  }
  
  public static class AddObstacle extends ClientGameDatagram {
    int x;
    int y;
    
    public AddObstacle(int x, int y) {
      this.x = x;
      this.y = y;
    }
    
    @Override
    public void _execute() {
      if (verbose) 
        System.out.println("Received AddObstacle(" + x + "," + y + ").");
      if (game.model.addObstacle(x, y))
        client.send(new ServerDatagram.Ack(client.id));
    }
  }
  
  public static class RemoveObstacle extends ClientGameDatagram {
    String coords;
    
    public RemoveObstacle(String c) {
      coords = c;
    }
    
    @Override
    public void _execute() {
      if (verbose)
        System.out.println("Received RemoveObstacle(" + coords + ").");
      game.model.removeObstacle(coords);
      client.send(new ServerDatagram.Ack(client.id));
    }
  }
  
  public static class AddPlayer extends ClientGameDatagram {
    int id;
    int x;
    int y;
    Color c;
    
    public AddPlayer(int id, int x, int y, Color c) {
      this.id = id;
      this.x = x;
      this.y = y;
      this.c = c;
    }
    
    @Override
    public void _execute() {
      if (verbose)
        System.out.println("Received AddPlayer(" + id + "," + x + "," + y + ").");
      game.model.addPlayer(id, x, y, c);
      client.send(new ServerDatagram.Ack(client.id));
    }
  }
  
  public static class MovePlayer extends ClientGameDatagram {
    int id;
    int x;
    int y;
    
    public MovePlayer(int id, int x, int y) {
      this.id = id;
      this.x = x;
      this.y = y;
    }
    
    @Override
    public void _execute() {
      if (verbose)
        System.out.println("Received MovePlayer(" + id + "," + x + "," + y + ").");
      game.model.movePlayer(id, x, y);
    }
  }
  
  public static class DisplacePlayer extends ClientGameDatagram {
    int id;
    int x;
    int y;

    public DisplacePlayer(int id, int x, int y) {
      this.id = id;
      this.x = x;
      this.y = y;
    }

    @Override
    public void _execute() {
      if (verbose)
        System.out.println("Received DisplacePlayer(" + id + "," + x + "," + y + ").");
      game.model.displacePlayer(id, x, y);
      client.send(new ServerDatagram.Ack(client.id));
    }
  }
  
  public static class PerformAStar extends ClientGameDatagram {
    int id;
    int h;

    public PerformAStar(int id, int h) {
      this.id = id;
      this.h = h;
    }

    @Override
    public void _execute() {
      game.model.performAStar(id, h);
      client.send(new ServerDatagram.Ack(client.id));
    }
  }
  
  public static class Lose extends ClientGameDatagram {
    @Override
    public void _execute() {
      JOptionPane.showMessageDialog(game.view, "You lose!");
      System.exit(0);
    }
  }
  
  public static class DeclareWinner extends ClientGameDatagram {
    int id;

    public DeclareWinner(int id) {
      this.id = id;
    }

    @Override
    public void _execute() {
      String message = id == client.id ? "You win!" : "You lose!";
      JOptionPane.showMessageDialog(game.view, message);
      game.view.dispose();
    }
  }
}
